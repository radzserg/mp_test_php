<?php

namespace App;

use Exception;
use League\Event\Emitter;

class EmailList
{

    const EVENT_ADD_SUBSCRIBER = 'email_list.add_subscriber';

    /**
     * @var string - subscriber's name
     */
    private $name;


    /**
     * @var Subscriber[] - array of list subscribers
     */
    private $subscribers = [];

    /**
     * @var Emitter
     */
    private $emitter;

    /**
     * EmailList constructor.
     * @param $name - name of th
     * @param Subscriber[] $subscribers
     * @throws Exception
     */
    public function __construct($name, $subscribers = [])
    {
        if (empty($name)) {
            throw new Exception("List name cannot be blank");
        }
        $this->name = $name;

        if ($subscribers) {
            foreach ($subscribers as $subscriber) {
                if (!$subscriber instanceof Subscriber) {
                    throw new Exception("All subscribers must be instances of App\\Subscriber");
                }
                $this->subscribers[$subscriber->getEmail()] = $subscriber;
            }
        }

        $this->emitter = new Emitter();
    }

    /**
     * Return list name
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Return subscribers list
     * @return Subscriber[]
     */
    public function getSubscribers()
    {
        return $this->subscribers;
    }

    /**
     * Adds event listeners to this list
     * @param $event
     * @param $listener
     */
    public function addListener($event, $listener)
    {
        $this->emitter->addListener($event, $listener);
    }

    /**
     * Adds subscriber to the list
     * @param Subscriber $subscriber
     */
    public function addSubscriber(Subscriber $subscriber)
    {
        if (isset($this->subscribers[$subscriber->getEmail()])) {
            return ;
        }
        $this->subscribers[$subscriber->getEmail()] = $subscriber;
        $this->emitter->emit(static::EVENT_ADD_SUBSCRIBER, $subscriber);
    }


}