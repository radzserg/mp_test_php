<?php

namespace App;

use Exception;

abstract class EmailCampaign
{

    /**
     * @var EmailMessage - email message to send
     */
    protected $emailMessage;

    /**
     * @var EmailList[]
     */
    protected $lists = [];

    /**
     * @var array - extra options
     */
    protected $options = [];

    /**
     * Newsletter constructor.
     * @param EmailMessage $emailMessage
     * @param $lists EmailList[]
     * @param array $options
     * @throws Exception
     */
    public function __construct(EmailMessage $emailMessage, $lists, $options = [])
    {
        $this->emailMessage = $emailMessage;

        if ($lists) {
            foreach ($lists as $list) {
                if (!$list instanceof EmailList) {
                    throw new Exception("All subscribers must be instances of App\\EmailList");
                }
            }
            $this->lists = $lists;
        }

        $this->options = $options;
        $this->validate();
    }

    /**
     * Starts campaign
     * @return mixed
     */
    abstract public function start();

    /**
     * Additional validation in child classes
     */
    protected function validate()
    {
    }

    /**
     * Return unique subscribers from all campaign lists so we don't sent same email N times to same recipient
     */
    protected function filterUniqueSubscribers()
    {
        $subscribers = [];
        foreach ($this->lists as $list) {
            $listSubscribers = $list->getSubscribers();
            foreach ($listSubscribers as $subscriber) {
                $subscribers[$subscriber->getEmail()] = $subscriber;
            }
        }

        return $subscribers;
    }


}