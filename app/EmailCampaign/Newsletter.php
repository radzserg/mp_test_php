<?php

namespace App\EmailCampaign;

use App\EmailCampaign;
use Exception;

class Newsletter extends EmailCampaign
{

    /**
     * @var - integer - timestamp to send email
     */
    private $sendTime;

    protected function validate()
    {
        $sendTime = isset($this->options['send_time']) ? $this->options['send_time'] : null;
        if (!$sendTime) {
            return ;
        }
        if (!is_integer($sendTime)) {
            throw new Exception('send_time param must be valid timestamp');
        }

        if ($sendTime < time()) {
            throw new Exception('send_time param must be greater than now');
        }

        $this->sendTime = $sendTime;
    }

    public function start()
    {
        $subscribers = $this->filterUniqueSubscribers();

        foreach ($subscribers as $subscriber) {
            $this->emailMessage->send($subscriber, $this->sendTime);
        }

    }

}
