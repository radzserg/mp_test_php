<?php

namespace App\EmailCampaign;

use App\EmailCampaign;
use App\EmailList;
use App\Subscriber;

class WelcomeEmail extends EmailCampaign
{


    public function start()
    {
        foreach ($this->lists as $emailList) {
            // for each list handle send email to user when he/she is added to list
            $emailList->addListener(EmailList::EVENT_ADD_SUBSCRIBER, function ($eventName, Subscriber $subscriber) {
                $this->emailMessage->send($subscriber);
            });
        }
    }

}
