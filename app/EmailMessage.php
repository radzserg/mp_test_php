<?php

namespace App;

use Exception;

class EmailMessage
{

    /**
     * @var string - email subject
     */
    private $subject;

    /**
     * @var string - email body
     */
    private $body;



    /**
     * EmailMessage constructor.
     * @param $subject - email subject
     * @param $body - email body
     * @throws Exception
     */
    public function __construct($subject, $body)
    {
        if (empty($subject)) {
            throw new Exception("Email subject cannot be blank");
        }

        if (empty($body)) {
            throw new Exception("Email body cannot be blank");
        }

        $this->subject = $subject;
        $this->body = $body;
    }

    /**
     * Return email subject
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Return email body
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Stub for email sending
     * @param Subscriber $subscriber
     * @param null $sendTime
     */
    public function send(Subscriber $subscriber, $sendTime = null)
    {
        $name = $subscriber->getName();
        $email = $subscriber->getEmail();
        $message = "Recipient: {$name} <{$email}>\n"
            . "Subject: {$this->subject}\n\n";
        if ($sendTime) {
            $message .= "Send Time: " . date('Y-m-d H:i:s', $sendTime) . "\n";
        }
        $message .= "{$this->body}\n\n\n";
        echo $message;
    }

}