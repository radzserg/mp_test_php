<?php

namespace App;

class Subscriber
{

    /**
     * @var - string subscriber's name
     */
    private $name;

    /**
     * @var - string subscriber's email
     */
    private $email;

    public function __construct($email, $name)
    {
        if (!$this->isValidEmail($email)) {
            throw new \Exception("Provided string {$email} is not a valid email address");
        }

        $this->email = trim($email);
        $this->name = trim($name);
    }

    /**
     * Return subscriber name
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Return subscriber email
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Validates email address
     * @param $email
     * @return bool
     */
    private function isValidEmail($email)
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL) !== false;
    }
}