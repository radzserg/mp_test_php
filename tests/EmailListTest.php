<?php

namespace App\Tests;

use App\EmailList;
use App\Subscriber;
use PHPUnit\Framework\TestCase;


final class EmailListTest extends TestCase
{


    public function testCreateValidEmptyList()
    {
        $name = 'Pro users';
        $list = new EmailList($name);
        $this->assertNotNull($list);

        $this->assertEquals($name, $list->getName());
    }

    public function testCreateInvalidListWithEmptyName()
    {
        $this->expectExceptionMessage("List name cannot be blank");
        new EmailList('');
    }

    public function testCreateList()
    {
        $name = 'Pro users';

        $subscribers = [];

        $limit = mt_rand(2, 10);
        for ($i = 1; $i <= $limit; $i++) {
            $subscribers[] = new Subscriber("email_{$i}@mail.com", "name_{$i}");
        }

        $list = new EmailList($name, $subscribers);
        $this->assertEquals($subscribers, array_values($list->getSubscribers()));
    }

    public function testCreateListWithInvalidSubscribers()
    {
        $this->expectExceptionMessage("All subscribers must be instances of App\\Subscriber");
        $name = 'Pro users';

        $subscribers = [
            'email_1@mail.com',
            'email_2@mail.com',
        ];

        new EmailList($name, $subscribers);
    }

    public function testAddSubscriber()
    {
        $subscribers = [];
        $limit = mt_rand(2, 10);
        for ($i = 1; $i <= $limit; $i++) {
            $subscribers[] = new Subscriber("email_{$i}@mail.com", "name_{$i}");
        }

        $list = new EmailList('Pro users', $subscribers);
        $listenerWereCalled = false;
        $list->addListener(EmailList::EVENT_ADD_SUBSCRIBER, function($event, $subscriber) use (&$listenerWereCalled) {
            $listenerWereCalled = $subscriber;
        });

        $newEmail = 'email_new@mail.com';
        $newSubscriber = new Subscriber($newEmail, "name_new");
        $list->addSubscriber($newSubscriber);

        $listSubscribers = $list->getSubscribers();
        $this->assertTrue(isset($listSubscribers[$newEmail]));
        $this->assertEquals($newSubscriber, $listSubscribers[$newEmail]);

        $this->assertEquals($newSubscriber, $listenerWereCalled);
    }


}

