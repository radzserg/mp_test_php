<?php

namespace App\Tests;

use App\EmailCampaign\Newsletter;
use App\EmailList;
use App\EmailMessage;
use App\Subscriber;
use PHPUnit\Framework\TestCase;

final class EmailCampaignTest extends TestCase
{


    public function testCreateValidCampaign()
    {
        $emailMessage = new EmailMessage('subject', 'body');
        $lists = [
            new EmailList('active users', [
                new Subscriber('tomas@mail.com', 'tomas')
            ])
        ];

        $campaign = new Newsletter($emailMessage, $lists, ['extra' => 'option']);
        $this->assertNotNull($campaign);
    }

    public function testCreateCampaignWithIncorrectLists()
    {
        $emailMessage = new EmailMessage('subject', 'body');
        $lists = [
            'list'
        ];

        $this->expectExceptionMessage("All subscribers must be instances of App\\EmailList");
        new Newsletter($emailMessage, $lists, ['extra' => 'option']);
    }


}

