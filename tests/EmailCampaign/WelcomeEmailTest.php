<?php

namespace App\Tests\EmailCampaign;

use App\EmailCampaign\WelcomeEmail;
use App\EmailList;
use App\EmailMessage;
use App\Subscriber;
use PHPUnit\Framework\TestCase;

final class WelcomeEmailTest extends TestCase
{

    public function testSendOnSubscribe()
    {
        $emailMessage = new EmailMessage('subject', 'some text here');
        $activeUsersList = new EmailList('active users');
        $proUsersList = new EmailList('pro users');
        $lists = [$activeUsersList, $proUsersList];

        $campaign = new WelcomeEmail($emailMessage, $lists);
        $campaign->start();

        $newEmail = 'email_new@mail.com';
        $newSubscriber = new Subscriber($newEmail, "name_new");

        $activeUsersList->addSubscriber($newSubscriber);

        $output = "Recipient: name_new <email_new@mail.com>\n"
            . "Subject: subject\n\n"
            . "some text here\n\n\n";

        $this->expectOutputString($output);
    }

}

