<?php

namespace App\Tests\EmailCampaign;

use App\EmailCampaign\Newsletter;
use App\EmailList;
use App\EmailMessage;
use App\Subscriber;
use PHPUnit\Framework\TestCase;

final class NewsletterTest extends TestCase
{

    public function testCreateCampaignWithStringDateSendTimeParam()
    {
        $emailMessage = new EmailMessage('subject', 'body');
        $lists = [
            new EmailList('active users', [
                new Subscriber('tomas@mail.com', 'tomas')
            ])
        ];

        $this->expectExceptionMessage("send_time param must be valid timestamp");
        new Newsletter($emailMessage, $lists, ['send_time' => '2016-03-12']);
    }

    public function testCreateCampaignWithPastSendTimeParam()
    {
        $emailMessage = new EmailMessage('subject', 'body');
        $lists = [
            new EmailList('active users', [
                new Subscriber('tomas@mail.com', 'tomas')
            ])
        ];

        $this->expectExceptionMessage("send_time param must be greater than now");
        new Newsletter($emailMessage, $lists, ['send_time' => strtotime("-1 minute")]);
    }

    public function testImmediateCampaign()
    {
        $emailMessage = new EmailMessage('subject', 'some text here');
        $lists = [
            new EmailList('active users', [
                new Subscriber('tomas@mail.com', 'tomas'),
                new Subscriber('john@mail.com', 'John'),
            ]),
            new EmailList('pro users', [
                new Subscriber('tomas@mail.com', 'tomas'),
                new Subscriber('bruce@mail.com', 'bruce')
            ]),
        ];


        $campaign = new Newsletter($emailMessage, $lists);
        $campaign->start();

        $output = "Recipient: tomas <tomas@mail.com>\n"
            . "Subject: subject\n\n"
            . "some text here\n\n\n"

            . "Recipient: John <john@mail.com>\n"
            . "Subject: subject\n\n"
            . "some text here\n\n\n"

            . "Recipient: bruce <bruce@mail.com>\n"
            . "Subject: subject\n\n"
            . "some text here\n\n\n";

        $this->expectOutputString($output);
    }

    public function testDelayedCampaign()
    {
        $emailMessage = new EmailMessage('subject', 'some text here');
        $lists = [
            new EmailList('active users', [
                new Subscriber('tomas@mail.com', 'tomas'),
                new Subscriber('john@mail.com', 'John'),
            ]),
            new EmailList('pro users', [
                new Subscriber('tomas@mail.com', 'tomas'),
                new Subscriber('bruce@mail.com', 'bruce')
            ]),
        ];


        $time = strtotime("+3 hours");
        $campaign = new Newsletter($emailMessage, $lists, ['send_time' => $time]);
        $campaign->start();

        $time = date('Y-m-d H:i:s', $time);
        $output = "Recipient: tomas <tomas@mail.com>\n"
            . "Subject: subject\n\n"
            . "Send Time: {$time}\n"
            . "some text here\n\n\n"

            . "Recipient: John <john@mail.com>\n"
            . "Subject: subject\n\n"
            . "Send Time: {$time}\n"
            . "some text here\n\n\n"

            . "Recipient: bruce <bruce@mail.com>\n"
            . "Subject: subject\n\n"
            . "Send Time: {$time}\n"
            . "some text here\n\n\n";

        $this->expectOutputString($output);
    }

}

