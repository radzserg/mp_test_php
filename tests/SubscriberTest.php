<?php

namespace App\Tests;

use App\Subscriber;
use PHPUnit\Framework\TestCase;


final class SubscriberTest extends TestCase
{

    public function testCreateValidSubscriber()
    {
        $email = 'tomas@email.com';
        $name = 'Tomas';
        $subscriber = new Subscriber($email, $name);
        $this->assertNotNull($subscriber);
        $this->assertEquals($subscriber->getEmail(), $email);
        $this->assertEquals($subscriber->getName(), $name);
    }

    public function testCreateSubscriberWithEmptyName()
    {
        $email = 'tomas@email.com';
        $name = null;
        $subscriber = new Subscriber($email, $name);
        $this->assertNotNull($subscriber);
        $this->assertEquals($subscriber->getEmail(), $email);
        $this->assertEquals($subscriber->getName(), $name);
    }

    public function testCreateSubscriberWithInvalidEmail()
    {
        $email = 'tomas';
        $this->expectExceptionMessage("Provided string {$email} is not a valid email address");
        new Subscriber($email, 'Tomas');
    }
}

