<?php

namespace App\Tests;

use App\EmailMessage;
use App\Subscriber;
use PHPUnit\Framework\TestCase;


final class EmailMessageTest extends TestCase
{


    public function testCreateValidEmail()
    {
        $subject = 'Welcome to My App';
        $body = "Hi <user>, welcome to my app";

        $emailMessage = new EmailMessage($subject, $body);
        $this->assertNotNull($emailMessage);

        $this->assertEquals($subject, $emailMessage->getSubject());
        $this->assertEquals($body, $emailMessage->getBody());
    }


    public function testCreateEmailWithEmptySubject()
    {
        $this->expectExceptionMessage("Email subject cannot be blank");
        new EmailMessage(null, 'body');
    }

    public function testCreateEmailWithEmptyBody()
    {
        $this->expectExceptionMessage("Email body cannot be blank");
        new EmailMessage('subject', null);
    }

    public function testSend()
    {
        $subject = 'Welcome to My App';
        $body = "Hi <user>, welcome to my app";

        $emailMessage = new EmailMessage($subject, $body);

        $email = 'tomas@email.com';
        $name = 'Tomas';
        $subscriber = new Subscriber($email, $name);

        $output = "Recipient: Tomas <tomas@email.com>\n"
            . "Subject: Welcome to My App\n\n"
            . "Hi <user>, welcome to my app\n\n\n";

        $this->expectOutputString($output);
        $emailMessage->send($subscriber);
    }

    public function testSendDelayed()
    {
        $subject = 'Welcome to My App';
        $body = "Hi <user>, welcome to my app";

        $emailMessage = new EmailMessage($subject, $body);

        $email = 'tomas@email.com';
        $name = 'Tomas';
        $subscriber = new Subscriber($email, $name);

        $time = strtotime("+3 hours");
        $timeFormatted = date('Y-m-d H:i:s', $time);
        $output = "Recipient: Tomas <tomas@email.com>\n"
            . "Subject: Welcome to My App\n\n"
            . "Send Time: {$timeFormatted}\n"
            . "Hi <user>, welcome to my app\n\n\n";

        $this->expectOutputString($output);


        $emailMessage->send($subscriber, $time);
    }
}

