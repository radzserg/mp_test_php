## 2. Sending newsletters

### install notes

```
  composer install
```

### demo

```
  php index.php
```

### tests

```
  composer run-script test
```

### Dev notes
I designed Subscriber and List classes. As for email model I split it into 2 classes - EmailMessage - component that store subject and body and can send email for provided Subscriber. And I also added extra classes - EmailCampaign-s. App\EmailCampaign\Newsletter - is a simple example - you can start campaign immediately or pass delay option. You can  Those classes a responsible for batch email sending. App\EmailCampaign\WelcomeEmail will add listeners for provided email lists and will handle specific event EmailList::EVENT_ADD_SUBSCRIBER. When new user added to list an event will ne emitted and WelcomeEmail will send EmailMessage to new Subscriber. As for notification email - it should handle outside events but in fact it does same job as Newsletter - it sends emails to its lists. I added an example in index.php that handles test event from outside with current timestamp and sends emails with this time to lists. 