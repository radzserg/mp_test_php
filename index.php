<?php

use App\EmailCampaign\Newsletter;
use App\EmailCampaign\WelcomeEmail;
use App\EmailList;
use App\EmailMessage;
use App\Subscriber;
use League\Event\Emitter;

require __DIR__ . '/vendor/autoload.php';

$activeUsers = new EmailList('active users', [
    new Subscriber('tomas@mail.com', 'tomas'),
    new Subscriber('john@mail.com', 'John'),
]);
$proUsers = new EmailList('pro users', [
    new Subscriber('nick@mail.com', 'tomas'),
    new Subscriber('bruce@mail.com', 'bruce')
]);

$lists = [$activeUsers, $proUsers];

echo "Create newsletter email campaign\n\n";

$emailMessage = new EmailMessage('Don\'t miss this news', 'news test here');
$campaign = new Newsletter($emailMessage, $lists);
$campaign->start();

echo "\n\n\n\nCreate notification email campaign\n\n";
// in fact we use newsletter campaign but send it when some system event occurs

const CONTENT_ADDED_EVENT = "content_added";
$emitter = new Emitter();
$emitter->addListener(CONTENT_ADDED_EVENT, function($eventName, $content) use ($lists) {
    $emailMessage = new EmailMessage('Don\'t miss this news', "news content created {$content}");
    $campaign = new Newsletter($emailMessage, $lists);
    $campaign->start();
});

$emitter->emit(CONTENT_ADDED_EVENT, time());


echo "\n\n\n\nCreate welcome email campaign\n\n";

$campaign = new WelcomeEmail($emailMessage, $lists);
$campaign->start();

$activeUsers->addSubscriber(new Subscriber('matt@mail.com', 'Matt'));
$proUsers->addSubscriber(new Subscriber('andy@mail.com', 'Andy'));